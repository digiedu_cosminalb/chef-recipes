name        "composer"
description 'Common composer actions'
maintainer  "Albulescu Cosmin"
maintainer_email "cosmin.albulescu@digital-education.com"
license     "Apache 2.0"
version     "1.0.0"

depends 'composer'