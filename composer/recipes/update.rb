include_recipe "composer"
node[:deploy].each do |application, deploy|
    composer_project "#{deploy[:deploy_to]}/current" do
      dev false
      quiet true
      action :update
    end
end