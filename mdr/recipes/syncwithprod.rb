node[:deploy].each do |application, deploy|
    execute "./console db:sync -y -o -i prod" do
        user deploy[:user]
        group deploy[:group]
        environment 'HOME' => "/home/#{deploy[:user]}"
        cwd "#{deploy[:deploy_to]}/current/app"
    end
end