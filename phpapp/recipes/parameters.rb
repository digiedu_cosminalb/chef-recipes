node[:deploy].each do |application, deploy|

  if deploy[:application_type] != 'php'
    Chef::Log.debug("phpapp:: application #{application} as it is not an PHP app")
    next
  end

  # write out opsworks.php
  template "#{deploy[:deploy_to]}/shared/config/database.yml" do
    cookbook 'phpapp'
    source 'database.yml.erb'
    mode '0660'
    owner deploy[:user]
    group deploy[:group]
    variables(
      :database => deploy[:database],
    )
    only_if do
      File.exists?("#{deploy[:deploy_to]}/shared/config")
    end
  end

  link "#{deploy[:deploy_to]}/current/app/config/parameters.yml" do
    to "#{deploy[:deploy_to]}/shared/config/database.yml"
    owner deploy[:user]
    group deploy[:group]
    mode '0755'
  end
end