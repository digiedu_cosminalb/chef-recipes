node[:deploy].each do |application, deploy|

    if deploy[:application_type] != 'php'
        Chef::Log.debug("composer::update - Not an php application")
        next
    end

    execute "Clear cache folder" do
      command "php app/console cache:clear"
      cwd "#{deploy[:deploy_to]}/current"
      user "deploy"
      group "www-data"
      action :run
    end
end