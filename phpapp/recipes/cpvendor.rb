node[:deploy].each do |application, deploy|

    if deploy[:application_type] != 'php'
        Chef::Log.debug("composer::update - Not an php application")
        next
    end

    execute "Copy vendor folder" do
      command "cp -r -a /home/deploy/vendor/ #{deploy[:deploy_to]}/current"
      user "deploy"
      group "www-data"
      action :run
    end
end