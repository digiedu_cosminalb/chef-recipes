node[:deploy].each do |application, deploy|

    if deploy[:application_type] != 'php'
        Chef::Log.debug("composer::update - Not an php application")
        next
    end

    execute "Dump assets" do
      command "php app/console assetic:dump --env=prod"
      cwd "#{deploy[:deploy_to]}/current"
      user deploy[:user]
      group deploy[:group]
      action :run
    end
end