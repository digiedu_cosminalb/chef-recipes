node[:deploy].each do |application, deploy|

    if deploy[:application_type] != 'php'
        Chef::Log.debug("composer::update - Not an php application")
        next
    end

    directory "#{deploy[:deploy_to]}/current/app/cache" do
      owner 'deploy'
      group 'www-data'
      mode '0777'
      action :create
    end

    directory "#{deploy[:deploy_to]}/current/app/logs" do
      owner 'deploy'
      group 'www-data'
      mode '0777'
      action :create
    end

    execute "Fix permissions for cache" do
      command "chmod -R 777 app/cache"
      cwd "#{deploy[:deploy_to]}/current"
      action :run
    end

    execute "Fix permissions for logs" do
      command "chmod -R 777 app/logs"
      cwd "#{deploy[:deploy_to]}/current"
      action :run
    end
end