include_recipe "grunt_cookbook"

node[:deploy].each do |application, deploy|
    
    node[:grunt_tasks].to_a.each do |item| 
        
        path = "#{deploy[:deploy_to]}/current/#{item[:path]}"

        grunt_cookbook_npm path do
          action :install
        end

        execute "bower install" do
            user deploy[:user]
            group deploy[:group]
            environment 'HOME' => "/home/#{deploy[:user]}"
            cwd path
        end

        grunt_cookbook_grunt path do
          action :task
          task item[:task]
        end
    end
end