if node.has_key?(:npm)
    if node[:npm].has_key?(:globals)

        node[:npm][:globals].each { |package|
            Chef::Log.info("Installing global node package: #{package}")
            execute "npm install -g #{package}" do
              action :run
            end
        }

    else
        Chef::Log.error("Please define npm.globals as array in stack json");
    end
else
    Chef::Log.error "Please define npm.globals in stack json";
end
